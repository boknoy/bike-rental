<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Traits\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
class LoginController extends Controller
{
    use AuthenticatesUsers;
    use ThrottlesLogins;

    protected $maxAttempts = 2;
	/**
 	* Number of minutes to lock the login.
 	*/
	protected $decayMinutes = 1; 	
 
	 
	public function login(Request $request)
    {
        $this->validateLogin($request);
       
        if (!$this->attemptLogin($request)) {    	
        	if ($this->hasTooManyLoginAttempts($request)) {
        		$this->fireLockoutEvent($request); //Fire the lockout event.
        		return $this->sendLockoutResponse($request); //redirect the user back after lockout.
    		}

    		$this->incrementLoginAttempts($request);
            return $this->sendFailedLoginResponse();
        }

     	return $this->sendLoginResponse($request);
    }

	public function logout(Request $request)
    {
        return response()->json(['request' => $request->user()]);
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
    public function register(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:users',
            'name' => 'required|',
            'password' => 'required'
        ]);

      
         $user  = new User;
         $user -> name = $request->name;
         $user -> email = $request->email;
         $user -> password =Hash::make($request->password);
         $user -> save();

         $user->assignRole($request->role);
         
         return response([
             'message' => 'Register Successfull',
             'user' => $user,
         ]);
    }
	
}
