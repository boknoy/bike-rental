<?php

namespace App\Http\Controllers;

use App\Models\BikeDetail;
use App\Models\User;
use Illuminate\Http\Request;
use DB;

class BikeDetailController extends Controller
{
    public function carousel($bikeid)
    {
    $BikeDetail = BikeDetail::select('*')
    ->get();
    return view('bikedetail',compact('BikeDetail'));
    }
}
