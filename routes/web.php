<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FileController;
use App\Http\Controllers\BikeDetailController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FrontpageController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	Route::get('/', function () {
		return view('frontpage');
	});
	Auth::routes();
	Route::get('about-us', function () {
		return view('user.about-us');
	});

	Route::get('contact', function () {
		return view('user.contact');
	});
	

    Route::view('login','auth/login');
    Route::view('register','auth/register');
    
    
        Route::view('admin','admin.layout');
	
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Route::get('/dashboard', [App\Http\Controllers\AdminController::class, 'index'])->name('dashboard');

//Route::get('/dashboard', [AdminController::class, 'index'])->name('home');


Route::get('user/home',function(){
	return view('auth/login');
})->middleware(['auth','verified'])->name('user/home');

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home')->middleware('auth');
Route::get('admin/dashboard', 'App\Http\Controllers\AdminController@index')->name('admin/dashboard')->middleware('auth');
Route::group(['middleware' => 'auth'], function () {
	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');

	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

	Route::get('map', function () {
		return view('pages.map');
	})->name('map');

	Route::get('notifications', function () {
		return view('pages.notifications');
	})->name('notifications');

	Route::get('rtl-support', function () {
		return view('pages.language');
	})->name('language');

	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});



Route::resource('bike_details',BikeDetailController::class);
Route::get('allbikes',[BikeDetailController::class,'allbikes']);

Route::get('reg-users',[RegisterController::class,'reg-users']);
Route::get('withdriver',[BikeDetailController::class,'withdriver']);
Route::get('withoutdriver',[BikeDetailController::class,'withoutdriver']);

Route::get('upload-bike',[BikeDetailController::class,'create']);
Route::get('bike-listing',[BikeDetailController::class,'index']);
Route::get('bikedetail/{bike_details}',[BikeDetailController::class,'show']);
Route::get('myposts/{bike_details}',[BikeDetailController::class,'userpost']);
Route::get('editbike/{BikeDetail}',[BikeDetailController::class,'edit']);
Route::get('deletebike/{bike_details}',[BikeDetailController::class,'destroy']);
Route::get('updatebike/{bike_details}',[BikeDetailController::class,'update']);
